import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CardRound extends StatelessWidget {
  const CardRound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 108,
      decoration: const BoxDecoration(
        color: AppColors.primaryLight,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10.0,
            // shadow
            spreadRadius: .5,
            // set effect of extending the shadow
            offset: Offset(0.0, 5.0),
          )
        ],
      ),
      child: Padding(
        padding:
            const EdgeInsets.symmetric(horizontal: AppStyles.defaultPadding),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(right: AppStyles.defaultPadding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Golf de nantes',
                      style: AppTextStyles.secondary18.copyWith(
                        fontWeight: FontWeight.w500,
                        color: AppColors.green500,
                      ),
                      maxLines: 2,
                    ),
                    const SizedBox(
                      height: AppStyles.defaultPadding / 2,
                    ),
                    Text(
                      '15 avril',
                      style: AppTextStyles.secondary16
                          .copyWith(color: AppColors.primaryDark),
                    ),
                    const SizedBox(
                      height: AppStyles.defaultPadding,
                    ),
                    Text(
                      '18 trous',
                      style: AppTextStyles.secondary14
                          .copyWith(color: AppColors.neutre500),
                    ),
                  ],
                ),
              ),
            ),
            Text(
              "+13",
              style: AppTextStyles.secondary18.copyWith(
                color: AppColors.green500,
                fontWeight: FontWeight.w500,
              ),
            )
          ],
        ),
      ),
    );
  }
}
