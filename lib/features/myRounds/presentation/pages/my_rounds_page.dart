import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/myRounds/presentation/widgets/card_round.dart';
import 'package:flutter/material.dart';

class MyRoundsPages extends StatelessWidget {
  const MyRoundsPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsetsDirectional.symmetric(
              horizontal: AppStyles.defaultPadding,
              vertical: AppStyles.defaultPadding,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "2023",
                  style: AppTextStyles.secondary30.copyWith(
                    color: AppColors.green500,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                const CardRound(),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                const CardRound(),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                const CardRound(),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                Text(
                  "2022",
                  style: AppTextStyles.secondary30.copyWith(
                    color: AppColors.green500,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                const CardRound(),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                const CardRound(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
