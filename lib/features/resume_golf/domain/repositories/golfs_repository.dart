import 'package:birdie/core/fp/result.dart';
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';

abstract class GolfsRepository {
  Future<Result<List<GolfEntity>>> getLocal();
}
