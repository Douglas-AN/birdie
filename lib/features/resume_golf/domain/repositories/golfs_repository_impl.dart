import 'package:birdie/core/fp/result.dart';
import 'package:birdie/features/resume_golf/data/datasources/golfs_local_datasource.dart';
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';

import 'package:birdie/features/resume_golf/domain/repositories/golfs_repository.dart';

class GolfsRepositoryImpl implements GolfsRepository {
  GolfsRepositoryImpl({
    required GolfsLocalDataSource golfsLocalDataSource,
  }) : _golfsLocalDataSource = golfsLocalDataSource;

  final GolfsLocalDataSource _golfsLocalDataSource;

  @override
  Future<Result<List<GolfEntity>>> getLocal() {
    return runCatchingAsync(() async {
      return _golfsLocalDataSource.getGolfs();
    });
  }
}
