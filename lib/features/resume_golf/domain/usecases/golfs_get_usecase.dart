import 'package:birdie/core/fp/result.dart';
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';
import 'package:birdie/features/resume_golf/domain/repositories/golfs_repository.dart';

class GolfsGetUseCase {
  GolfsGetUseCase({
    required GolfsRepository golfsRepository,
  }) : _golfsRepository = golfsRepository;

  final GolfsRepository _golfsRepository;

  Future<Result<List<GolfEntity>>> call() async {
    return _golfsRepository.getLocal();
  }
}
