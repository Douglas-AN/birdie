import 'package:birdie/features/resume_golf/data/models/golf_model.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'golf_entity.freezed.dart';
part 'golf_entity.g.dart';

@freezed
class GolfEntity with _$GolfEntity {
  const factory GolfEntity({
    required String id,
    required String label,
    required String description,
    required String city,
    required String image,
  }) = _GolfEntity;

  factory GolfEntity.fromJson(Map<String, dynamic> json) =>
      _$GolfEntityFromJson(json);

  factory GolfEntity.fromModel(GolfModel model) => GolfEntity(
        id: model.id,
        label: model.label,
        description: model.description,
        city: model.city,
        image: model.image,
      );
}
