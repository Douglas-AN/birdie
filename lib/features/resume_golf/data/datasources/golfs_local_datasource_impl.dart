import 'package:birdie/features/resume_golf/data/datasources/golfs_local_datasource.dart';
import 'dart:convert';
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';

class GolfsLocalDataSourceImpl implements GolfsLocalDataSource {
  GolfsLocalDataSourceImpl();

  @override
  Future<List<GolfEntity>> getGolfs() async {
    late List<GolfEntity> golfs = (json.decode(
      ('/features/resume_golf/data/datasources/golfs.json'),
    ) as List<dynamic>)
        .map((dynamic e) => GolfEntity.fromJson(e as Map<String, dynamic>))
        .toList();

    return golfs;
  }
}
