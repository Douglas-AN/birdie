import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';

abstract class GolfsLocalDataSource {
  Future<List<GolfEntity>> getGolfs();
}
