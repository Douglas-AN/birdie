import 'package:freezed_annotation/freezed_annotation.dart';

part 'golf_model.freezed.dart';
part 'golf_model.g.dart';

@freezed
class GolfModel with _$GolfModel {
  const factory GolfModel({
    required String id,
    required String label,
    required String description,
    required String city,
    required String image,
  }) = _GolfModel;

  factory GolfModel.fromJson(Map<String, dynamic> json) =>
      _$GolfModelFromJson(json);
}
