import 'package:birdie/core/domain/enum/button_type.dart';
import 'package:birdie/core/theme/button.dart';
import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/configure_game_play/presentation/pages/configure_game_play_page.dart';

import 'package:birdie/features/resume_golf/presentation/widgets/golf_card.dart';

import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';

class ResumeGolfPage extends StatefulWidget {
  const ResumeGolfPage({super.key});
  static Route<void> route() {
    return MaterialPageRoute<void>(
      settings: const RouteSettings(name: '/resumeGolf'),
      builder: (_) => const ResumeGolfPage(),
    );
  }

  @override
  State<ResumeGolfPage> createState() => _ResumeGolfPageState();
}

class _ResumeGolfPageState extends State<ResumeGolfPage> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        toolbarHeight: 75,
        centerTitle: true,
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(50.0),
          child: Padding(
            padding:
                const EdgeInsets.only(bottom: AppStyles.defaultPadding * 2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Icon(
                  Icons.location_on,
                  color: AppColors.primaryDark,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: AppStyles.defaultPadding,
                  ),
                  child: Text(
                    'Nantes',
                    style: AppTextStyles.secondary24.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.qr_code,
              color: AppColors.primaryDark,
            ),
            onPressed: () {},
          )
        ],
      ),
      // body: BlocBuilder<GolfsCubit, GolfsState>(
      //   builder: (_, GolfsState golfsState) {
      //     // late List<GolfCard> cards;

      //     golfsState.whenOrNull(
      //       search: (List<GolfEntity> golfs) =>
      //           cards = _golfCards(context, golfs),
      //       error: (_) {
      //         WidgetsBinding.instance.addPostFrameCallback(
      //           (_) {
      //             const SnackBar(content: Text('Erreur'));
      //           },
      //         );
      //       },
      //     );
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: SizedBox(
              height: 475,
              child: PageView.builder(
                itemCount: 3,
                controller: PageController(viewportFraction: 0.85),
                onPageChanged: (int index) => setState(() => _index = index),
                itemBuilder: (_, i) {
                  return Transform.scale(
                    scale: i == _index ? 1 : 0.95,
                    child: GolfCard(
                      image: AppImages.golf1,
                      name: appLocalizations.golfNameNantes,
                      description: appLocalizations.golfDescNantes,
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
      //   },
      // ),
      bottomNavigationBar: SizedBox(
        height: 130,
        child: Padding(
          padding: const EdgeInsetsDirectional.symmetric(
            horizontal: AppStyles.defaultPadding,
          ),
          child: Column(
            children: <Widget>[
              Button(
                label: appLocalizations.bookingButton,
                type: ButtonType.secondary,
                onClick: () => Navigator.pop(context),
              ),
              const SizedBox(
                height: AppStyles.defaultPadding,
              ),
              Button(
                label: appLocalizations.joinGolfButton,
                type: ButtonType.primary,
                onClick: () => Navigator.of(context)
                    .pushReplacement(ConfigureGamePlayPage.route()),
              )
            ],
          ),
        ),
      ),
    );
  }

  // List<GolfCard> _golfCards(BuildContext context, List<GolfEntity> golfs) {
  //   return golfs
  //       .map(
  //         (GolfEntity golf) => GolfCard(
  //           golf: golf,
  //         ),
  //       )
  //       .toList();
  // }
}
