import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/common/presentation/widgets/avatar_circle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class GolfCard extends StatelessWidget {
  const GolfCard({
    super.key,
    // required this.golf,
    required this.image,
    required this.name,
    required this.description,
  });

  final String image;
  final String name;
  final String description;
  // final GolfEntity golf;

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Card(
      semanticContainer: true,
      margin: const EdgeInsets.all(10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 5,
      child: SizedBox(
        height: 200,
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.3),
                BlendMode.srcOver,
              ),
              image: AssetImage(image),
              fit: BoxFit.fill,
              alignment: Alignment.topCenter,
            ),
          ),
          child: Padding(
            padding: const EdgeInsetsDirectional.all(
              AppStyles.defaultPadding,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  name,
                  style: AppTextStyles.secondary30.copyWith(
                    color: AppColors.neutre50,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                Text(
                  description,
                  style: AppTextStyles.secondary16.copyWith(
                    color: AppColors.neutre50,
                  ),
                ),
                const SizedBox(
                  height: AppStyles.defaultPadding,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    const Expanded(
                      flex: 1,
                      child: Stack(
                        children: <Widget>[
                          AvatarCircle(image: AppImages.bois3, size: 40),
                          Positioned(
                            left: 20,
                            child:
                                AvatarCircle(image: AppImages.bois3, size: 40),
                          ),
                          Positioned(
                            left: 40,
                            child:
                                AvatarCircle(image: AppImages.bois3, size: 40),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        appLocalizations.nbFriendInGolf(3),
                        style: AppTextStyles.secondary14.copyWith(
                          color: AppColors.neutre50,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
    // return Card(
    //   elevation: 6,
    //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    //   child: Center(
    //     child: Text(
    //       golf.label,
    //       style: const TextStyle(fontSize: 32),
    //     ),
    //   ),
    // );
  }
}
