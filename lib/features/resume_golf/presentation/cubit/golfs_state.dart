
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'golfs_state.freezed.dart';

@freezed
class GolfsState with _$GolfsState {
  const factory GolfsState.initial() = GolfsStateInitial;
  const factory GolfsState.loading() = GolfsStateLoading;
  const factory GolfsState.empty() = GolfsStateEmpty;
  const factory GolfsState.loaded() = GolfsStateLoaded;
  const factory GolfsState.search({
    required List<GolfEntity> golfs,
  }) = GolfsStateSearch;

  const factory GolfsState.error({
    required Exception exception,
  }) = GolfsStateError;
}
