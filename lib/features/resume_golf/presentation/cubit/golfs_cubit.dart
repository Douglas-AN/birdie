import 'package:birdie/core/fp/result.dart';
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';
import 'package:birdie/features/resume_golf/domain/usecases/golfs_get_usecase.dart';
import 'package:birdie/features/resume_golf/presentation/cubit/golfs_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GolfsCubit extends Cubit<GolfsState> {
  GolfsCubit({
    required GolfsGetUseCase golfsGetUseCase,
  })  : _golfsGetUseCase = golfsGetUseCase,
        super(const GolfsState.initial());

  final GolfsGetUseCase _golfsGetUseCase;

  Future<void> get() async {
    emit(const GolfsState.loading());

    final Result<List<GolfEntity>> result = await _golfsGetUseCase.call();

    emit(
      result.when(
        success: (List<GolfEntity> golfs) => golfs.isEmpty
            ? const GolfsState.empty()
            : GolfsState.search(golfs: golfs),
        failure: (Exception exception) =>
            GolfsState.error(exception: exception),
      ),
    );
  }
}
