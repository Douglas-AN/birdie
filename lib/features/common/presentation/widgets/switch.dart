import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

class SwitchApp extends StatelessWidget {
  const SwitchApp({
    super.key,
    this.active = true,
    required this.onChange,
  });

  final bool active;
  final Function(bool) onChange;

  @override
  Widget build(BuildContext context) {
    return FlutterSwitch(
      value: active,
      width: 40,
      height: 20,
      toggleSize: AppStyles.defaultPadding,
      padding: 2,
      activeColor: AppColors.green500,
      inactiveColor: AppColors.neutre500,
      activeTextColor: AppColors.neutre50,
      inactiveTextColor: AppColors.neutre50,
      onToggle: onChange,
    );
  }
}
