import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    super.key,
    required this.label,
    required this.padding,
    required this.value,
    required this.onChanged,
    this.disable = false,
  });

  final String label;
  final EdgeInsets padding;
  final bool value;
  final ValueChanged<bool> onChanged;
  final bool disable;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => disable ? null : onChanged(!value),
      child: Padding(
        padding: padding,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                label,
                style: AppTextStyles.secondary16.copyWith(
                  color: disable ? AppColors.neutre300 : AppColors.primaryDark,
                ),
              ),
            ),
            Checkbox(
              activeColor: disable ? AppColors.neutre75 : AppColors.green500,
              value: value,
              onChanged: disable
                  ? null
                  : (bool? newValue) {
                      onChanged(newValue!);
                    },
            ),
          ],
        ),
      ),
    );
  }
}
