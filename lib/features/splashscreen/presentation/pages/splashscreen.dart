import 'dart:async';

import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/home/presentation/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashscreenPage extends StatefulWidget {
  const SplashscreenPage({super.key});
  static Route<void> route() {
    return MaterialPageRoute<void>(
      settings: const RouteSettings(name: '/splash'),
      builder: (_) => const SplashscreenPage(),
    );
  }

  @override
  State<SplashscreenPage> createState() => _SplashscreenState();
}

class _SplashscreenState extends State<SplashscreenPage>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  int count = 0;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );

    _controller.stop();

    _controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        count++;
        if (count < 1) {
          _controller.reset();
          _controller.forward();
        }
      }
    });

    Timer(
      const Duration(seconds: 2),
      () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const HomePage()),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryLight,
      body: Stack(children: <Widget>[
        Center(
          child: _appIcon(),
        ),
      ]),
    );
  }

  Widget _appIcon() {
    return Padding(
      padding: const EdgeInsetsDirectional.all(AppStyles.defaultPadding * 2),
      child: SvgPicture.asset(AppImages.logo),
    );
  }
}
