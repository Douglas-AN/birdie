import 'package:birdie/core/constants/global_constants.dart';
import 'package:birdie/core/domain/enum/button_type.dart';
import 'package:birdie/core/theme/button.dart';
import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/common/presentation/widgets/avatar_circle.dart';
import 'package:birdie/features/personnal/presentation/widgets/card_data_medium.dart';
import 'package:birdie/features/resume_golf/presentation/pages/resume_golf_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class PersonnalPage extends StatelessWidget {
  const PersonnalPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Stack(
      children: [
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: AppStyles.defaultPadding,
            ),
            child: Column(
              children: [
                const SizedBox(
                  height: 53,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: AppStyles.defaultPadding * 3),
                      child: Text(
                        appLocalizations.sayHello(GlobalConstants.name),
                        style: AppTextStyles.secondary30.copyWith(
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    const AvatarCircle(image: AppImages.driver, size: 60),
                  ],
                ),
                const SizedBox(
                  height: AppStyles.defaultPadding * 2,
                ),
                Button(
                  label: appLocalizations.playButton,
                  type: ButtonType.primary,
                  onClick: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ResumeGolfPage()),
                  ),
                ),
                const SizedBox(
                  height: AppStyles.defaultPadding * 2,
                ),
                GridView.count(
                  crossAxisCount: 2,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  // Generate 100 widgets that display their index in the List.
                  children: <Widget>[
                    CardDataMedium(
                      image: AppImages.green1,
                      label: appLocalizations.index,
                      data: 37,
                    ),
                    CardDataMedium(
                      image: AppImages.green2,
                      label: appLocalizations.levelPlay,
                      data: 18,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
