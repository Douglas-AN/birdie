import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';

class CardDataMedium extends StatelessWidget {
  const CardDataMedium({
    super.key,
    required this.image,
    required this.label,
    required this.data,
  });
  final String image;
  final String label;
  final double data;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 170,
          width: 170,
          decoration: BoxDecoration(
            color: AppColors.neutre50,
            borderRadius: const BorderRadius.all(Radius.circular(30)),
            boxShadow: const [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 10.0,
                // shadow
                spreadRadius: .1,
                // set effect of extending the shadow
                offset: Offset(10.0, 10.0),
              )
            ],
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
              alignment: Alignment.bottomLeft,
            ),
          ),
          child: Flexible(
            child: Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      top: AppStyles.defaultPadding,
                      bottom: AppStyles.defaultPadding * 2,
                    ),
                    child: Text(
                      label,
                      style: AppTextStyles.secondary18.copyWith(
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  Text(
                    textAlign: TextAlign.center,
                    data.toString(),
                    style: AppTextStyles.secondary24.copyWith(
                      fontWeight: FontWeight.w700,
                      color: AppColors.primaryLight,
                    ),
                  ),
                ],
              ),
            ),
          ),
          //       child: Column(
          //         children: [
          //           Padding(
          //             padding: const EdgeInsets.only(top: AppStyles.defaultPadding),
          //             child: Text(
          //               label,
          //               style: AppTextStyles.secondary18.copyWith(
          //                 fontWeight: FontWeight.w500,
          //               ),
          //             ),
          //           ),

          //           Text(
          //             textAlign: TextAlign.center,
          //             data.toString(),
          //             style: AppTextStyles.secondary24.copyWith(
          //               fontWeight: FontWeight.w700,
          //               color: AppColors.green500,
          //             ),
          //           ),

          //           // Flexible(
          //           //   child: Align(
          //           //     alignment: AlignmentDirectional.bottomStart,
          //           //     child: Image.asset(
          //           //       image,
          //           //       height: 130,
          //           //       width: 130,
          //           //     ),
          //           //   ),
          //           // ),
          //         ],
          //       )),
        ),
      ],
    );
  }
}
