import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/common/presentation/widgets/checkbox_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class PlayedType extends StatefulWidget {
  const PlayedType({Key? key}) : super(key: key);

  @override
  State<PlayedType> createState() => _PlayedTypeState();
}

class _PlayedTypeState extends State<PlayedType> {
  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    late bool _isSelected = false;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          appLocalizations.playedTypeTitle,
          style: AppTextStyles.secondary18.copyWith(
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(
          height: AppStyles.defaultPadding / 2,
        ),
        LabeledCheckbox(
          label: appLocalizations.playTypeAmical,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          value: _isSelected,
          onChanged: (bool newValue) {
            setState(() {
              _isSelected = newValue;
            });
          },
        ),
        const Divider(height: 0),
        LabeledCheckbox(
          label: appLocalizations.playTypeAmicalCompetition,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          value: _isSelected,
          onChanged: (bool newValue) {
            setState(() {
              _isSelected = newValue;
            });
          },
        ),
        const Divider(height: 0),
        LabeledCheckbox(
          label: appLocalizations.playTypeCompetition,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          value: _isSelected,
          disable: true,
          onChanged: (bool newValue) {
            setState(() {
              _isSelected = newValue;
            });
          },
        ),
        const Divider(height: 0),
      ],
    );
  }
}
