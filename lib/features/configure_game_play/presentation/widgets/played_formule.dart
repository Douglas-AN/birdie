import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/common/presentation/widgets/checkbox_label.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class PlayedFormule extends StatefulWidget {
  const PlayedFormule({Key? key}) : super(key: key);

  @override
  State<PlayedFormule> createState() => _PlayedFormuleState();
}

class _PlayedFormuleState extends State<PlayedFormule> {
  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    late bool isSelected1 = false;
    late bool isSelected2 = false;
    late bool isSelected3 = false;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          appLocalizations.playedFormuleTitle,
          style: AppTextStyles.secondary18.copyWith(
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(
          height: AppStyles.defaultPadding / 2,
        ),
        LabeledCheckbox(
          label: appLocalizations.playTypeStableford,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          value: isSelected1,
          onChanged: (bool? newValue) {
            setState(() {
              isSelected1 = newValue!;
            });
          },
        ),
        const Divider(height: 0),
        LabeledCheckbox(
          label: appLocalizations.playTypeMatchPlay,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          value: isSelected2,
          onChanged: (bool? newValue) {
            setState(() {
              isSelected2 = newValue!;
            });
          },
        ),
        const Divider(height: 0),
        LabeledCheckbox(
          label: appLocalizations.playTypeStrokePlay,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          value: isSelected3,
          onChanged: (bool? newValue) {
            setState(() {
              isSelected3 = newValue!;
            });
          },
        ),
        const Divider(height: 0),
      ],
    );
  }
}
