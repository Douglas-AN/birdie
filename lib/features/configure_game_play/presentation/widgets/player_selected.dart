import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/common/presentation/widgets/avatar_circle.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class PlayerSelected extends StatelessWidget {
  const PlayerSelected({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                appLocalizations.playerSelectedTitle,
                style: AppTextStyles.secondary18.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Text(
                appLocalizations.editButton,
                style: AppTextStyles.secondary16.copyWith(
                  decoration: TextDecoration.underline,
                  color: AppColors.green500,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: AppStyles.defaultPadding * 1.5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const AvatarCircle(
                  size: 40,
                  image: AppImages.driver,
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: AppStyles.defaultPadding),
                  child: Text(
                    'toto',
                    style: AppTextStyles.secondary16,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(right: AppStyles.defaultPadding),
                  child: Text(
                    '18',
                    style: AppTextStyles.secondary16
                        .copyWith(color: AppColors.neutre700),
                  ),
                ),
                SizedBox(
                  width: 18,
                  height: 18,
                  child: ClipOval(
                    child: SizedBox.fromSize(
                      size: const Size.fromRadius(48), // Image radius
                      child: Container(
                        color: AppColors.yellow500,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
        const SizedBox(
          height: AppStyles.defaultPadding,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const AvatarCircle(
                  size: 40,
                  image: AppImages.bois3,
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: AppStyles.defaultPadding),
                  child: Text(
                    'titi',
                    style: AppTextStyles.secondary16,
                  ),
                )
              ],
            ),
            Row(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(right: AppStyles.defaultPadding),
                  child: Text(
                    '18',
                    style: AppTextStyles.secondary16
                        .copyWith(color: AppColors.neutre700),
                  ),
                ),
                SizedBox(
                  width: 18,
                  height: 18,
                  child: ClipOval(
                    child: SizedBox.fromSize(
                      size: const Size.fromRadius(48), // Image radius
                      child: Container(
                        color: AppColors.primaryDark,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ],
    );
  }
}
