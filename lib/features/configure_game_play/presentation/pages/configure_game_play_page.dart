import 'package:birdie/core/domain/enum/button_type.dart';
import 'package:birdie/core/theme/button.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/configure_game_play/presentation/widgets/played_formule.dart';
import 'package:birdie/features/configure_game_play/presentation/widgets/played_type.dart';
import 'package:birdie/features/configure_game_play/presentation/widgets/player_selected.dart';
import 'package:birdie/features/play/presentation/pages/play_page.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';

class ConfigureGamePlayPage extends StatefulWidget {
  const ConfigureGamePlayPage({super.key});
  static Route<void> route() {
    return MaterialPageRoute<void>(
      settings: const RouteSettings(name: '/configureGamePlay'),
      builder: (_) => const ConfigureGamePlayPage(),
    );
  }

  @override
  State<ConfigureGamePlayPage> createState() => _ConfigureGamePlayPage();
}

class _ConfigureGamePlayPage extends State<ConfigureGamePlayPage> {
  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 75,
        centerTitle: true,
        backgroundColor: Colors.transparent,
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(50.0),
          child: Padding(
            padding:
                const EdgeInsets.only(bottom: AppStyles.defaultPadding * 2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Icon(
                  Icons.location_on,
                  color: AppColors.primaryDark,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: AppStyles.defaultPadding,
                  ),
                  child: Text(
                    'Golf de Nantes',
                    style: AppTextStyles.secondary24.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.share,
              color: AppColors.primaryDark,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: const Padding(
        padding: EdgeInsets.symmetric(horizontal: AppStyles.defaultPadding),
        child: Column(
          children: <Widget>[
            PlayerSelected(),
            SizedBox(
              height: AppStyles.defaultPadding,
            ),
            PlayedFormule(),
            SizedBox(
              height: AppStyles.defaultPadding,
            ),
            PlayedType(),
          ],
        ),
      ),
      bottomNavigationBar: SizedBox(
        height: 84,
        child: Padding(
          padding: const EdgeInsetsDirectional.symmetric(
            horizontal: AppStyles.defaultPadding,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Button(
                width: 155,
                label: appLocalizations.cancelButton,
                type: ButtonType.secondary,
                onClick: () {},
              ),
              const SizedBox(
                height: AppStyles.defaultPadding,
              ),
              Button(
                width: 155,
                label: appLocalizations.continueButton,
                type: ButtonType.primary,
                onClick: () =>
                    Navigator.of(context).pushReplacement(PlayPage.route()),
              )
            ],
          ),
        ),
      ),
    );
  }
}
