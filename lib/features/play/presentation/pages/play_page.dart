import 'package:birdie/core/constants/global_constants.dart';
import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/play/presentation/widgets/club_choice.dart';
import 'package:birdie/features/play/presentation/widgets/end_play.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:flutter/material.dart';

class PlayPage extends StatefulWidget {
  const PlayPage({super.key});
  static Route<void> route() {
    return MaterialPageRoute<void>(
      settings: const RouteSettings(name: '/play'),
      builder: (_) => const PlayPage(),
    );
  }

  @override
  State<PlayPage> createState() => _PlayPage();
}

class _PlayPage extends State<PlayPage> {
  int _index = 0;
  late bool isOpen = false;

  @override
  void initState() {
    super.initState();
    isOpen = false;
  }

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return SafeArea(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(93),
          child: Align(
            alignment: const Alignment(0.0, 1),
            child: Container(
              height: MediaQuery.of(context).size.height / 14.5,
              padding: const EdgeInsetsDirectional.symmetric(
                  horizontal: AppStyles.defaultPadding),
              child: Container(
                width: double.infinity,
                height: 40,
                decoration: BoxDecoration(
                  color: AppColors.primaryLight,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 20.0,
                      // shadow
                      spreadRadius: .5,
                      // set effect of extending the shadow
                      offset: Offset(0.0, 5.0),
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      icon: const Icon(
                        Icons.arrow_back,
                        color: AppColors.primaryDark,
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                    SizedBox(
                      height: 24,
                      width: 300,
                      child: PageView.builder(
                        itemCount: GlobalConstants.nbHoleHaflGolf,
                        controller: PageController(viewportFraction: 0.18),
                        onPageChanged: (int index) =>
                            setState(() => _index = index),
                        itemBuilder: (_, i) {
                          WidgetsBinding.instance
                              .addPostFrameCallback((_) async {
                            if (i + 1 == 9 && isOpen != true) {
                              isOpen = true;
                              await showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  builder: (context) {
                                    return const EndPlay();
                                  });
                            }
                          });

                          return Transform.scale(
                            scale: i == _index ? 1 : 0.9,
                            child: Text(
                              "${i + 1}",
                              style: AppTextStyles.secondary24.copyWith(
                                fontWeight: FontWeight.w500,
                                color: i == _index
                                    ? AppColors.green500
                                    : AppColors.neutre300,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        body: Positioned(
          top: 0,
          child: Image.asset(
            AppImages.play,
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
          ),
        ),
        bottomNavigationBar: Container(
          color: AppColors.green500,
          height: 84,
          child: Padding(
            padding: const EdgeInsetsDirectional.symmetric(
              horizontal: AppStyles.defaultPadding,
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      showModalBottomSheet(
                        isScrollControlled: true,
                        context: context,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        builder: (context) {
                          return const ClubChoice();
                        },
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        Text(
                          appLocalizations.clubPlayed,
                          style: AppTextStyles.secondary18.copyWith(
                            fontWeight: FontWeight.w500,
                            color: AppColors.primaryLight,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          'Driver',
                          style: AppTextStyles.secondary16
                              .copyWith(color: AppColors.primaryLight),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: <Widget>[
                        Text(
                          appLocalizations.hitInProgress,
                          style: AppTextStyles.secondary18.copyWith(
                            fontWeight: FontWeight.w500,
                            color: AppColors.primaryLight,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          '1',
                          style: AppTextStyles.secondary16.copyWith(
                            color: AppColors.primaryLight,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: <Widget>[
                        Text(
                          appLocalizations.par,
                          style: AppTextStyles.secondary18.copyWith(
                            fontWeight: FontWeight.w500,
                            color: AppColors.primaryLight,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          '5',
                          style: AppTextStyles.secondary16
                              .copyWith(color: AppColors.primaryLight),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
