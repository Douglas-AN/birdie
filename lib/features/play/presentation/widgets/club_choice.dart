import 'package:birdie/core/theme/images.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class ClubChoice extends StatelessWidget {
  const ClubChoice({super.key});

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Container(
      height: MediaQuery.of(context).size.height - 75,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: SingleChildScrollView(
        child: Wrap(
          children: [
            Align(
              child: Padding(
                padding: const EdgeInsets.only(top: 60, bottom: 36),
                child: Text(
                  appLocalizations.clubChoiceTitle,
                  style: AppTextStyles.secondary24.copyWith(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            GridView.count(
              crossAxisCount: 3,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              // Generate 100 widgets that display their index in the List.
              children: List.generate(13, (index) {
                return Center(
                  child: _cardClub(),
                );
              }),
            ),
          ],
        ),
      ),
    );
  }

  Widget _cardClub() {
    return Container(
      height: 120,
      width: 120,
      decoration: const BoxDecoration(
        color: AppColors.primaryLight,
        borderRadius: BorderRadius.all(Radius.circular(30)),
      ),
      alignment: Alignment.center,
      child: Wrap(
        direction: Axis.horizontal,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 12.5),
            child: Text(
              'Driver',
              style: AppTextStyles.secondary18.copyWith(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Image.asset(AppImages.driver),
        ],
      ),
    );
  }
}
