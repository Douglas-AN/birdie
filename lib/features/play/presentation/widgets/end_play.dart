import 'package:birdie/core/domain/enum/button_type.dart';
import 'package:birdie/core/theme/button.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/common/presentation/widgets/switch.dart';
import 'package:birdie/features/home/presentation/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class EndPlay extends StatelessWidget {
  const EndPlay({super.key});

  @override
  Widget build(BuildContext context) {
    final I10n appLocalizations = I10n.of(context)!;
    return Container(
      height: MediaQuery.of(context).size.height - 75,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: SingleChildScrollView(
        child: Wrap(
          children: [
            Align(
              child: Padding(
                padding: const EdgeInsets.only(top: 60, bottom: 36),
                child: Text(
                  appLocalizations.endPlayTitle,
                  style: AppTextStyles.secondary24.copyWith(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppStyles.defaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _smallCardInfo(appLocalizations.score, 33),
                      _smallCardInfo(appLocalizations.nbPutt, 2.3),
                      _smallCardInfo(appLocalizations.indexSimulation, 29),
                    ],
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  _bigCardInfo(appLocalizations.progressPoints),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  const Divider(
                    height: 0,
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        appLocalizations.giveChallenge,
                        style: AppTextStyles.secondary18.copyWith(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SwitchApp(
                        active: false,
                        onChange: (bool active) {
                          !active;
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  const Divider(
                    height: 0,
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  Text(
                    appLocalizations.paramPlay,
                    style: AppTextStyles.secondary18.copyWith(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        appLocalizations.federationRegister,
                        style: AppTextStyles.secondary16,
                      ),
                      SwitchApp(
                        onChange: (bool active) {
                          !active;
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        appLocalizations.indexCalcul,
                        style: AppTextStyles.secondary16,
                      ),
                      SwitchApp(
                        onChange: (bool active) {
                          !active;
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        appLocalizations.shotRegister,
                        style: AppTextStyles.secondary16,
                      ),
                      SwitchApp(
                        active: false,
                        onChange: (bool active) {
                          !active;
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding,
                  ),
                  Button(
                    label: appLocalizations.validePlay,
                    onClick: () =>
                        Navigator.of(context).pushReplacement(HomePage.route()),
                  ),
                  const SizedBox(
                    height: AppStyles.defaultPadding * 2,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _smallCardInfo(String title, double dataInfo) {
    return Container(
      height: 110,
      width: 110,
      decoration: const BoxDecoration(
        color: AppColors.neutre50,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
            // shadow
            spreadRadius: .5,
            // set effect of extending the shadow
            offset: Offset(0.0, 5.0),
          )
        ],
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 12.5),
            child: Text(
              title,
              style: AppTextStyles.secondary18.copyWith(
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Text(
            dataInfo.toString(),
            textAlign: TextAlign.center,
            style: AppTextStyles.secondary24.copyWith(
              fontWeight: FontWeight.w500,
              color: AppColors.green500,
            ),
          )
        ],
      ),
    );
  }

  Widget _bigCardInfo(String title) {
    return Container(
      height: 250,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: AppColors.neutre50,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 5.0,
            // shadow
            spreadRadius: .5,
            // set effect of extending the shadow
            offset: Offset(0.0, 5.0),
          )
        ],
      ),
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(
          AppStyles.defaultPadding,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: AppTextStyles.secondary18.copyWith(
                fontWeight: FontWeight.w500,
              ),
            ),
            Button(
              label: 'voir plus',
              type: ButtonType.secondary,
              onClick: () {},
            )
          ],
        ),
      ),
    );
  }
}
