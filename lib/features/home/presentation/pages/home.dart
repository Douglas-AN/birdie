import 'package:birdie/core/theme/styles.dart';
import 'package:birdie/features/home/presentation/widgets/bottom_navigation/bottom_navigator.dart';
import 'package:birdie/features/myRounds/presentation/pages/my_rounds_page.dart';
import 'package:birdie/features/personnal/presentation/pages/personnal_page.dart';
import 'package:birdie/features/resume_golf/presentation/cubit/golfs_cubit.dart';
import 'package:birdie/features/training/presentation/pages/training_page.dart';
import 'package:birdie/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  static Route<void> route() {
    return MaterialPageRoute<void>(
      settings: const RouteSettings(name: '/home'),
      builder: (_) => const HomePage(),
    );
  }

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<NavigatorItem>? items;
  NavigatorItem? currentItem;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<GolfsCubit>(context).get();
    final I10n appLocalizations = I10n.of(MyApp.navigKey.currentContext!)!;

    items = <NavigatorItem>[
      NavigatorItem(
        icon: Icons.date_range,
        body: const TrainingPage(),
        appBar: AppBar(
          toolbarHeight: 75,
          centerTitle: true,
          backgroundColor: Colors.transparent,
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(50.0),
            child: Padding(
              padding:
                  const EdgeInsets.only(bottom: AppStyles.defaultPadding * 2),
              child: Text(
                appLocalizations.trainingTitle,
                style: AppTextStyles.secondary24.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ),
      ),
      const NavigatorItem(
        icon: Icons.home_outlined,
        body: PersonnalPage(),
        appBar: null,
      ),
      NavigatorItem(
        icon: Icons.update,
        body: const MyRoundsPages(),
        appBar: AppBar(
          toolbarHeight: 75,
          centerTitle: true,
          backgroundColor: Colors.transparent,
          iconTheme: const IconThemeData(
            color: Colors.black, //change your color here
          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(50.0),
            child: Padding(
              padding:
                  const EdgeInsets.only(bottom: AppStyles.defaultPadding * 2),
              child: Text(
                appLocalizations.myRoundsTitle,
                style: AppTextStyles.secondary24.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ),
      ),
    ];
    currentItem = items![1];
  }

  @override
  Widget build(BuildContext context) {
    return items != null && currentItem != null
        ? Scaffold(
            appBar: currentItem!.appBar,
            body: BottomNavigator(
              items: items!,
              selectedItem: currentItem,
              onSelectItem: (int index) =>
                  WidgetsBinding.instance.addPostFrameCallback(
                (_) {
                  setState(() => currentItem = items![index]);
                },
              ),
            ),
          )
        : Container();
  }
}
