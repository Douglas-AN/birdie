import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';


class NavigatorTab extends StatelessWidget {
  const NavigatorTab({
    super.key,
    required this.icon,
    required this.selected,
  });

  final IconData icon;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(AppStyles.defaultPadding),
      child: Column(
        children: <Widget>[
          Icon(icon),
        ],
      ),
    );
  }
}
