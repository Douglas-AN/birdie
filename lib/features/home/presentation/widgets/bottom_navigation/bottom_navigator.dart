import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';

class NavigatorItem {
  const NavigatorItem({
    required this.icon,
    required this.body,
    required this.appBar,
  });

  final IconData icon;
  final Widget body;
  final AppBar? appBar;
}

class BottomNavigator extends StatefulWidget {
  const BottomNavigator({
    super.key,
    required this.items,
    this.selectedItem,
    this.onSelectItem,
  });

  final List<NavigatorItem> items;
  final NavigatorItem? selectedItem;
  final void Function(int)? onSelectItem;

  @override
  State<StatefulWidget> createState() => _BottomNavigatorState();
}

class _BottomNavigatorState extends State<BottomNavigator>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: widget.items.length,
      vsync: this,
    );
    _tabController
        .addListener(() => widget.onSelectItem?.call(_tabController.index));
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  late TabController _tabController;

  @override
  Widget build(BuildContext context) {
    if (widget.selectedItem != null &&
        widget.items.indexOf(widget.selectedItem!) != _tabController.index) {
      _tabController.animateTo(widget.items.indexOf(widget.selectedItem!));
    }
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Column(
          children: <Widget>[
            Expanded(
              child: TabBarView(
                controller: _tabController,
                physics: const NeverScrollableScrollPhysics(),
                children:
                    widget.items.map((NavigatorItem tab) => tab.body).toList(),
              ),
            ),
            ColoredBox(
              color: AppColors.neutre50,
              child: TabBar(
                controller: _tabController,
                labelColor: AppColors.green500,
                unselectedLabelColor: AppColors.neutre200,
                indicator: const UnderlineTabIndicator(
                  borderSide: BorderSide(color: AppColors.green500, width: 2.0),
                  insets: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 56.0),
                ),
                tabs: widget.items
                    .map(
                      (NavigatorItem e) => Tab(
                        height: 56,
                        iconMargin: const EdgeInsets.only(bottom: 4.0),
                        icon: Icon(
                          e.icon,
                          size: 25,
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        );
      },
    );
  }
}
