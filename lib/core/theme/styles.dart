import 'package:flutter/material.dart';

class AppStyles {
  static const double defaultPadding = 16.0;
}

class AppColors {
  static MaterialColor get primary {
    return MaterialColor(neutre50.value, const <int, Color>{
      50: neutre50,
      100: neutre50,
      200: neutre50,
      300: neutre50,
      400: neutre50,
      500: neutre50,
      600: neutre50,
      700: neutre50,
      800: neutre50,
      900: neutre50,
    });
  }

  // primary
  static const Color primaryDark = Color(0xFF1B2732);
  static const Color primaryLight = Color(0xFFF4F3EA);
  static const Color secondaryLight = Color(0xFFe2d8cd);

  // neutral
  static const Color neutre50 = Color(0xFFFFFFFF);
  static const Color neutre75 = Color(0xFFFAFAFA);
  static const Color neutre200 = Color(0xFFE9EBEC);
  static const Color neutre300 = Color(0xFFDEE0E3);
  static const Color neutre500 = Color(0xFFAEB1B7);
  static const Color neutre600 = Color(0xFF868A93);
  static const Color neutre700 = Color(0xFF60616C);
  static const Color neutre900 = Color(0xFF101014);

  // feedback
  static const Color green200 = Color(0xFF8FC1BB);
  static const Color green500 = Color(0xFF006356);
  static const Color red500 = Color(0xFFD02E26);
  static const Color blue500 = Color(0xFF4093D0);
  static const Color yellow500 = Color(0xFFFFBE23);
  static const Color purple500 = Color(0xFF8C3E83);
  static const Color orange500 = Color(0xFFDF8230);

  static const Color bg = Color(0xFFF5F5F5);
}

class AppTextStyles {
  // proximanova
  static TextStyle secondary30 = const TextStyle(
    fontSize: 30,
    fontFamily: 'proximanova',
    color: AppColors.primaryDark,
  );

  static TextStyle secondary24 = const TextStyle(
    fontSize: 24,
    fontFamily: 'proximanova',
    color: AppColors.primaryDark,
  );

  static TextStyle secondary18 = const TextStyle(
    fontSize: 18,
    fontFamily: 'proximanova',
    color: AppColors.primaryDark,
  );

  static TextStyle secondary16 = const TextStyle(
    fontSize: 16,
    fontFamily: 'proximanova',
    color: AppColors.primaryDark,
  );

  static TextStyle secondary14 = const TextStyle(
    fontSize: 14,
    fontFamily: 'proximanova',
    color: AppColors.primaryDark,
  );

  static TextStyle secondary12 = const TextStyle(
    fontSize: 12,
    fontFamily: 'proximanova',
    color: AppColors.primaryDark,
  );
}
