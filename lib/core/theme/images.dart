class AppImages {
  static const green1 = 'assets/images/greens/green1.png';
  static const green2 = 'assets/images/greens/green2.png';
  static const driver = 'assets/images/clubs/driver.png';
  static const bois3 = 'assets/images/clubs/bois3.png';
  static const play = 'assets/images/play.jpg';
  static const logo = 'assets/images/logo/logo.svg';
  static const logoLight = 'assets/images/logo/logo_light.svg';
  static const golf1 = 'assets/images/golfs/golf1.jpeg';
}
