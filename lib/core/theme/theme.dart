import 'package:flutter/material.dart';
import 'package:birdie/core/theme/styles.dart';

ThemeData theme() {
  return ThemeData(
    brightness: Brightness.light,
    colorScheme: ColorScheme.fromSwatch(primarySwatch: AppColors.primary)
        .copyWith(secondary: AppColors.green500),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        elevation: MaterialStateProperty.all<double>(0.0),
        shape: MaterialStateProperty.all<OutlinedBorder>(
          const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(6.0)),
          ),
        ),
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
          const EdgeInsets.all(14),
        ),
      ),
    ),
    appBarTheme: const AppBarTheme(
      shadowColor: Colors.transparent,
      centerTitle: true,
      titleSpacing: 0,
      // shape: Border(
      //   bottom: BorderSide(color: AppColors.neutre200),
      // ),
    ),

    scaffoldBackgroundColor: AppColors.bg,
    snackBarTheme: const SnackBarThemeData(
      shape: BorderDirectional(),
      behavior: SnackBarBehavior.floating,
      elevation: 0,
      backgroundColor: AppColors.neutre900,
    ),
    // hide the bubble under cursor
    textSelectionTheme: const TextSelectionThemeData(
      cursorColor: AppColors.neutre900,
      selectionColor: AppColors.green500,
      selectionHandleColor: Colors.transparent,
    ),
  );
}
