import 'package:birdie/core/domain/enum/button_type.dart';
import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({
    super.key,
    this.label,
    this.onClick,
    this.disable = false,
    this.width,
    this.icon,
    this.height = 40.0,
    this.iconSize = 20.0,
    this.iconPositionLeft = false,
    this.radius = 6.0,
    this.type = ButtonType.primary,
  });

  final String? label;
  final void Function()? onClick;
  final bool disable;
  final double? width;
  final IconData? icon;
  final double height;
  final double iconSize;
  final bool iconPositionLeft;
  final double radius;
  final ButtonType type;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? double.infinity,
      height: height,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: type == ButtonType.secondary ? 0 : 2,
          backgroundColor: type.backgroundColor,
          foregroundColor: type.backgroundColor,
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
            side: type == ButtonType.secondary
                ? const BorderSide(color: AppColors.green500)
                : BorderSide.none,
          ),
        ),
        onPressed: disable ? null : onClick,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (icon != null && iconPositionLeft)
              Padding(
                padding: EdgeInsets.only(
                  right: label != null ? (AppStyles.defaultPadding / 5) : 0,
                ),
                child: Icon(
                  icon,
                  size: iconSize,
                  color: type.itemColor,
                ),
              ),
            if (label != null)
              Text(
                label!,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: AppTextStyles.secondary14.copyWith(
                  fontWeight: FontWeight.w600,
                  color: disable ? AppColors.neutre500 : type.itemColor,
                ),
              ),
            if (icon != null && !iconPositionLeft)
              Padding(
                padding: EdgeInsets.only(
                  left: label != null ? (AppStyles.defaultPadding / 2) : 0,
                ),
                child: Icon(
                  icon,
                  size: iconSize,
                  color: type.itemColor,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
