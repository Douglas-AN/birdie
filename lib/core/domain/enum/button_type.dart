import 'package:birdie/core/theme/styles.dart';
import 'package:flutter/material.dart';

enum ButtonType {
  primary,
  secondary,
  icon,
}

extension ButtonTypeExtension on ButtonType {
  Color? get backgroundColor {
    switch (this) {
      case ButtonType.primary:
        return AppColors.green500;
      case ButtonType.secondary:
        return Colors.transparent;
      case ButtonType.icon:
        return Colors.transparent;
    }
  }

  Color? get itemColor {
    switch (this) {
      case ButtonType.primary:
        return AppColors.primaryLight;
      case ButtonType.secondary:
        return AppColors.green500;
      case ButtonType.icon:
        return AppColors.primaryDark;
    }
  }
}
