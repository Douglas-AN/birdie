import 'package:birdie/features/resume_golf/data/datasources/golfs_local_datasource.dart';
import 'package:birdie/features/resume_golf/data/datasources/golfs_local_datasource_impl.dart';
import 'package:birdie/features/resume_golf/domain/repositories/golfs_repository.dart';
import 'package:birdie/features/resume_golf/domain/repositories/golfs_repository_impl.dart';
import 'package:birdie/features/resume_golf/domain/usecases/golfs_get_usecase.dart';
import 'package:birdie/features/resume_golf/presentation/cubit/golfs_cubit.dart';
import 'package:get_it/get_it.dart';

final GetIt sl = GetIt.instance;

Future<void> init() async {
  await initGetGolfs();
}

Future<void> initGetGolfs() async {
  // datasource
  sl.registerLazySingleton<GolfsLocalDataSource>(
    () => GolfsLocalDataSourceImpl(),
  );
  //repository
  sl.registerLazySingleton<GolfsRepository>(
    () => GolfsRepositoryImpl(
      golfsLocalDataSource: sl(),
    ),
  );
  // usecase
  sl.registerLazySingleton<GolfsGetUseCase>(
    () => GolfsGetUseCase(golfsRepository: sl()),
  );
  // cubit
  sl.registerLazySingleton<GolfsCubit>(
    () => GolfsCubit(golfsGetUseCase: sl()),
  );
}
