import 'package:birdie/core/theme/theme.dart';
import 'package:birdie/features/resume_golf/presentation/cubit/golfs_cubit.dart';
import 'package:birdie/features/splashscreen/presentation/pages/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});
  static final GlobalKey<NavigatorState> navigKey = GlobalKey<NavigatorState>();

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<GolfsCubit>(
          create: (_) => di.sl<GolfsCubit>(),
        ),
      ],
      child: MaterialApp(
        theme: theme(),
        home: const SplashscreenPage(),
        navigatorKey: MyApp.navigKey,
        localizationsDelegates: I10n.localizationsDelegates,
        supportedLocales: const <Locale>[Locale('fr', '')],
      ),
    );
  }
}
