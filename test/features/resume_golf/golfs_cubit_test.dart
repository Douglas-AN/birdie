import 'dart:convert';

import 'package:birdie/core/fp/result.dart';
import 'package:birdie/features/resume_golf/domain/entities/golf_entity.dart';
import 'package:birdie/features/resume_golf/domain/usecases/golfs_get_usecase.dart';
import 'package:birdie/features/resume_golf/presentation/cubit/golfs_cubit.dart';
import 'package:birdie/features/resume_golf/presentation/cubit/golfs_state.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../fixtures/fixture_reader.dart';
import 'golfs_cubit_test.mocks.dart';

@GenerateMocks(<Type>[GolfsGetUseCase])
void main() {
  late GolfsGetUseCase golfsGetUseCase;
  late GolfsCubit golfsCubit;
  late List<GolfEntity> getGolf;
  late Exception exception;

  setUp(() async {
    golfsGetUseCase = MockGolfsGetUseCase();
    golfsCubit = GolfsCubit(golfsGetUseCase: golfsGetUseCase);

    getGolf = (json.decode(
      await readFixturesAsString('resume_golf/golf.json'),
    ) as List<Map<String, dynamic>>)
        .map((dynamic e) => GolfEntity.fromJson(e as Map<String, dynamic>))
        .toList();

    exception = Exception('error');
  });

  group('Get', () {
    test('initial state should be GolfsStateInitial', () {
      expect(
        golfsCubit.state,
        equals(const GolfsState.initial()),
      );
    });

    blocTest(
      ('Should get with success'),
      setUp: () {
        when(
          golfsGetUseCase.call(),
        ).thenAnswer(
          (_) async => Result<List<GolfEntity>>.success(data: getGolf),
        );
      },
      build: () => golfsCubit,
      act: (GolfsCubit golfsCubit) {
        golfsCubit.get();
      },
      expect: () => <GolfsState>[
        const GolfsState.loading(),
        GolfsState.search(golfs: getGolf)
      ],
    );

    blocTest(
      ('Should get without result'),
      setUp: () {
        when(
          golfsGetUseCase.call(),
        ).thenAnswer(
          (_) async => const Result<List<GolfEntity>>.success(
            data: <GolfEntity>[],
          ),
        );
      },
      build: () => golfsCubit,
      act: (GolfsCubit golfsCubit) {
        golfsCubit.get();
      },
      expect: () => <GolfsState>[
        const GolfsState.loading(),
        const GolfsState.empty(),
      ],
    );
    blocTest(
      ('Should get error'),
      setUp: () {
        when(
          golfsGetUseCase.call(),
        ).thenAnswer(
          (_) async => Result<List<GolfEntity>>.failure(
            exception: exception,
          ),
        );
      },
      build: () => golfsCubit,
      act: (GolfsCubit golfsCubit) {
        golfsCubit.get();
      },
      expect: () => <GolfsState>[
        const GolfsState.loading(),
        GolfsState.error(exception: exception),
      ],
    );
  });
}
