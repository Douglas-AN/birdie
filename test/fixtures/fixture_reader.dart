import 'dart:io';
import 'dart:typed_data';

Future<Uint8List> readFixtures(String path) {
  if (Directory.current.path.endsWith('/test')) {
    Directory.current = Directory.current.parent;
  }
  return File('test/fixtures/$path').readAsBytes();
}

Future<String> readFixturesAsString(String path) {
  if (Directory.current.path.endsWith('/test')) {
    Directory.current = Directory.current.parent;
  }
  return File('test/fixtures/$path').readAsString();
}
