# birdie

A fluter project base on clean Architecture.

## Exemple to understand clean architecture :

```bash
https://devmuaz.medium.com/flutter-clean-architecture-series-part-1-d2d4c2e75c47#:~:text=The%20Clean%20Architecture%20is%20the,that%20communicate%20through%20defined%20interfaces.
```

## Problèmes rencontrés

Grace à la mise en place de la clean architecture, le state, cubit, usecase, repo, ainsi que la datasource, ont étés misent en place, mais impossible de fetché sur le json localement. J'ai voulu mettre en place un appel au moins local pour pouvoir montrer comme j'aurrais géré ce genre d'appel. Suite à ce problème je ne peux donc pas faire remonter d'information dynamic à ma page. La logique reste en place mais serra donc simuler pour avoir un rendu au niveau de l'affichage de la "selection" des golfs.
Un autre problème, lors de la mise en place du test du cubit est également arrivé. Toujours sur le fetch de donnée du json.

J'ai également rencontrer un problème sur la mise en place de l'affichage de la page de fin de partie. Lorsque l'on scroll les numéro pour changer de trous, la page de fin s'affiche avant le dernier trous. Hors, dans le cas présent, elle devrait s'afficher que sur le numéro 9.

Egalement impossible de bien orchestrer la position des green sur les cards présentent sur la home.

## Installation du projet

### Configuration [FVM](https://pub.dev/packages/fvm)

1. Installation [Dart](https://www.dartlang.org/install).
2. Activation [FVM](https://fvm.app):

   ```bash
   dart pub global activate fvm
   ```

### Installation de la version de Flutter spécifique au projet

```bash
fvm install
```

### Installation des dépendances

1. Récupération des dépendances:

```bash
fvm flutter pub get
```

2. Construction des fichiers générés:

```bash
fvm flutter pub run build_runner build --delete-conflicting-outputs
```

## Lancement de l'application en local (via VsCode)

1. Sélectionner le device ou le simulateur sur lequel lancer l'application.
2. Run > Start Debugging ou taper F5.
3. Attendre le lancement de l'app.

En ligne de commandes :

```bash
fvm flutter run --flavor generic
```

## Montée de versions de Flutter et des dépendances

1. Liste des versions de Flutter disponibles via FVM:

```bash
fvm releases
```

2. Sélection de la version souhaitée:

```bash
fvm use <version>
```

1.  Il est possible de mettre à jour les dépendances à la main dans le fichier pubspec.yaml ou automatiquement via la commande :

```bash
fvm flutter pub upgrade
```

4. Pour identifier les dépendances qui ne sont pas à jour, il existe la commande :

```bash
fvm flutter pub outdated
```

5. Vérifier et corriger les errors et les warning du linter.

6. Corriger les problèmes de dépendances

7. Lancer les tests :

```bash
fvm flutter test
```

8. Lancer l'application pour vérifier d'éventuelles régressions.
